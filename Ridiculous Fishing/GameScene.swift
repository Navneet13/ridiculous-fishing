//
//  GameScene.swift
//  Ridiculous Fishing
//
//  Created by Navpreet Kaur on 2019-10-23.
//  Copyright © 2019 Navneet. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    //Sprites
    var fish: SKSpriteNode!
    var fishHook: SKSpriteNode!
    var scoreSprite: SKLabelNode!
    
    //Other Variables
    var numOfLoops = 0
    var fishesArray: [SKSpriteNode] = []
    var hookMoveDirection: SKAction!
    var hookedFishes: [SKSpriteNode] = []
    var spread = false
    var totalScore = 0
    
    let hookVerticalSpeed = 10
    let hookHorizontalSpeed = 5
    
    //MARK: didMove()
    override func didMove(to view: SKView) {
        self.fishHook = self.childNode(withName: "fishHook") as? SKSpriteNode
        self.scoreSprite = self.childNode(withName: "score") as? SKLabelNode
    }
    
    // MARK: update()
    override func update(_ currentTime: TimeInterval) {
        
        self.scoreSprite.text = "Score: \(totalScore)"
        if !spread {
            numOfLoops = numOfLoops + 1
            if(numOfLoops % 30 == 0) {
                self.spawnFishes()
            }
            
            // Detect collision
            for theFish in fishesArray {
                if(fishHook.frame.intersects(theFish.frame)) {
                    theFish.removeFromParent()
                    fishesArray.remove(at: fishesArray.firstIndex(of: theFish)!)
                    theFish.position = CGPoint(x: 0, y: 0)
                    theFish.removeAllActions()
                    self.fishHook.addChild(theFish)
                }
            }
            
            // Detect Hook Posiiton
            let yPosition = self.fishHook.position.y
            if yPosition <= 100 {
                hookMoveDirection = SKAction.moveTo(y: self.size.height - 400, duration: 10)
                self.fishHook.run(hookMoveDirection)
                self.fishHook.run(action: hookMoveDirection, withKey: "move", optionalCompletion: {
                    self.fishHook.removeAllActions()
                    self.spread = true
                    self.spreadFishes()
                })
            }
        }
    }
    
    private func spreadFishes() {
        self.hookedFishes = [SKSpriteNode]()
        for child in self.fishHook.children {
            if let fish = child as? SKSpriteNode {
                let randomX = CGFloat.random(in: 100...(self.size.width - 100))
                let randomY = CGFloat.random(in: self.size.height - 400...self.size.height + 200)
                fish.position.x = randomX
                fish.position.y = randomY
                fish.removeFromParent()
                self.addChild(fish)
                self.hookedFishes.append(fish)
                let moveDownDirection = SKAction.moveTo(y: 100, duration: 7)
                fish.run(action: moveDownDirection, withKey: "move", optionalCompletion: {
                    fish.removeFromParent()
                    self.hookedFishes.remove(at: self.hookedFishes.firstIndex(of: fish)!)
                })
            }
        }
    }
    
    // MARK: touchesBegan()
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if !spread {
            hookMoveDirection = SKAction.moveTo(y: 0, duration: 10)
            self.fishHook.run(hookMoveDirection)
            
            guard let mousePosition = touches.first?.location(in: self) else {
                return
            }
            
            let middleOfScreen  = self.size.width / 2
            
            // Move hook to the left
            if (mousePosition.x < middleOfScreen) {
                //Move to the left
                let moveLeftDirection = SKAction.moveTo(x: 100, duration: 2)
                self.fishHook.run(moveLeftDirection)
            }
                //Move hook to the right
            else {
                let moveRightDirection = SKAction.moveTo(x: self.size.width - 100, duration: 2)
                self.fishHook.run(moveRightDirection)
            }
        } else {
            guard let touchPosition = touches.first?.location(in: self) else {
                return
            }
            
            for fish in hookedFishes {
                let touchedNode = self.atPoint(touchPosition)
                if touchedNode == fish {
                    fish.removeFromParent()
                    hookedFishes.remove(at: hookedFishes.firstIndex(of: fish)!)
                    
                    if fish.name == "fish1" || fish.name == "fish3" || fish.name == "fish4"{
                        totalScore = totalScore + 10
                    } else if fish.name == "fish2" || fish.name == "fish5" {
                        totalScore = totalScore - 5
                    } else if fish.name == "fish6" {
                        totalScore = totalScore + 50
                    }
                }
            }
        }
    }
    
    // MARK: spawnFishes()
    func spawnFishes() {
        
        let randomFish = Int.random(in: 1...6)
        self.fish = SKSpriteNode(imageNamed: "fish\(randomFish)")
        self.fish.name = "fish\(randomFish)"
        let randomXPos = CGFloat.random(in: -100 ... size.width+100)
        let randomYPos = CGFloat.random(in: 0 ... size.height/2-50)
        
        self.fish.position = CGPoint(x:randomXPos, y:randomYPos)
        
        //Moving fishes in random directions
        let randomDirection = Int.random(in: 1...2)
        if randomDirection == 1 {
            let moveLeftDirection = SKAction.moveTo(x: -400, duration:  15)
            self.fish.run(moveLeftDirection)
        }
        else {
            let facingRight = SKAction.scaleX(to: -1, duration: 0)
            self.fish.run(facingRight)
            let moveRightDirection = SKAction.moveTo(x: size.width+100, duration:  15)
            self.fish.run(moveRightDirection)
        }
        
        addChild(self.fish)
        
        // Add the fishes to the array
        self.fishesArray.append(self.fish)
    }
}

extension SKNode {
    func run(action: SKAction!, withKey: String!, optionalCompletion:(() -> Void)?) {
        if let completion = optionalCompletion {
            let completionAction = SKAction.run(completion)
            let compositeAction = SKAction.sequence([ action, completionAction ])
            run(compositeAction, withKey: withKey )
        }
        else {
            run( action, withKey: withKey )
        }
    }
    
    func actionForKeyIsRunning(key: String) -> Bool {
        return self.action(forKey: key) != nil ? true : false
    }
}
